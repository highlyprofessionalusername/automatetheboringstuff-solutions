#!/usr/bin/env python3

print('Hello world!')
print('What is your name?')
myname = input()
print(f'Nice to meet you, {myname}')
print('The length of your name is ' + str(len(myname)))
print('What is your age?')
myage = input()
print(f'You will be ' + str(int(myage) + 1) + ' in a year')
