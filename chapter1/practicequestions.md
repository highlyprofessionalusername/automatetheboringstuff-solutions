# Practice Questions, Ch 1

Q1: Which of the following are operators, and which are values?

```python
*
'hello'
-88.8
-
/
+
5
```

A1:
`*`, `-`, `/` and `+` are operators.
`'hello'`, `-88.8` and `5` are values.

Q2: Which of the following is a variable, and which is a string?

```python
spam
'spam'
```

A2:
`spam` is a variable.
`'spam'` is a string.
The presence of single quotes `''` or double quotes `""` indicates a string.

Q3: Name three data types.

A3:
integers or `int`:
Any sequence of digits, either positive or negative

floating point numbers or float:
Any sequence of digits, either positive or negative, containing a decimal point.

string or `str`:
Any sequence of letters, numbers or other characters, delimited by either single quotes `''` or double quotes `""`.
Strings can be formatted, or raw.
Raw strings do not process escape sequences (like `'\n'`) but render them literally.
Formatted strings allow you to insert all valid Python expressions into a string.

boolean or `bool`:
The values `True` or `False` (note case sensitivity)

`list`:
A changeable and ordered comma separated sequence of elements, delimited by square brackets `[]`
Items in a list can be any data type, including lists containing lists, dictionaries or tuples.

dictionary or `dict`:
A changeable and unordered comma separated sequence of key-value pairs, delimited by curly brackets `{}`
Key-value pairs in a dictionary map the key to its associated value.

`tuple`:
An unchangeable and ordered comma separated sequence of elements, delimited by parentheses `()`
Items in a tuple can be any data type, including tuples of tuples, lists or dictionaries.

Q4: What is an expression made up of? What do all expressions do?

A4:
Expressions consist of values and operators, all of which can be evaluated down to a single value.
Single values with no operators are also values, but evaluate to themselves.

Q5: This chapter introduced assignment statements, like `spam = 10`. What is the difference between an expression and a statement?

A5:Statements consist of a variable `spam`, an assignment operator `=`, and a value `10`
Statements store values, and expressions evaluate values.

Q6: What does the variable bacon contain after the following code runs?

```python
bacon = 20
bacon + 1
```

A6:
The variable `bacon` contains the value `20`.
The reason for this is that the second line of code contains no logic to update the contents of the variable bacon.
To change this we would write the code as `bacon = bacon + 1`

Q7: What should the following two expressions evaluate to?

```python
'spam' + 'spamspam'
'spam' * 3
```

A7:
Both expressions evaluate to `spamspamspam` as they are using string concatenation and replication.

Q8: Why is `eggs` a valid variable name while `100` is invalid?

A8:
Because variables cannot begin with a number.

Q9: What three functions can be used to get the integer, floating-point number, or string version of a value?

A9:
`int(value)` to get the integer version
`float(value)` to get the floating point version
`str(value)` to get the string version

Q10: Why does this expression cause an error? How can you fix it?

```python
I have eaten ' + 99 + ' burritos.
```

A10:
`99` is evaluated as an integer due to the lack of single quote delimiters. You cannot concatenate strings with integers.
To fix this, delimit 99 with single quotes (or double quotes) to make it a string, or use `str()`
