#!/usr/bin/env python3

allguests = {'Heidi': {'apples': 5, 'bananas': 4},
             'Ben': {'cauliflowers': 20, 'bananas': 3},
             'Uda': {'beer': 50, 'pretzels': 20}}

def totalbrought(guests, item):
    numbrought = 0
    for k, v in guests.items():
        numbrought = numbrought + v.get(item, 0)
    return numbrought


print('Number of things being brought')
print('- apples        ' + str(totalbrought(allguests, 'apples')))
print('- bananas       ' + str(totalbrought(allguests, 'bananas')))
print('- cauliflowers  ' + str(totalbrought(allguests, 'cauliflowers')))
print('- beer          ' + str(totalbrought(allguests, 'beer')))
print('- pretzels      ' + str(totalbrought(allguests, 'pretzels')))
