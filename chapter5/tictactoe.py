#!/usr/bin/env python3

theBoard = {'top-L': ' ', 'top-M': ' ', 'top-R': ' ',
            'mid-L': ' ', 'mid-M': ' ', 'mid-R': ' ',
            'low-L': ' ', 'low-M': ' ', 'low-R': ' '}

def printboard(board):
    print(board['top-L'] + '|' + board['top-M'] + '|' + board['top-R'])
    print('-+-+-')
    print(board['mid-L'] + '|' + board['mid-M'] + '|' + board['mid-R'])
    print('-+-+-')
    print(board['low-L'] + '|' + board['low-M'] + '|' + board['low-R'])

turn = 'X'
completed_moves = 1
while completed_moves <10:
    printboard(theBoard)
    print('Turn for ' + turn + '. Move on which space?')
    move = input()
    try:
        if theBoard[move] != ' ':
            print('Field occupied')
            continue
        else:
            theBoard[move] = turn
            print(f'move {completed_moves}')
            completed_moves = (completed_moves + 1)
        if turn == 'X':
            turn = 'O'
        else:
            turn = 'X'
    except KeyError:
        print("Oops, that's not a valid field, try again!")

printboard(theBoard)
