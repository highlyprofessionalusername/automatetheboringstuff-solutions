#!/usr/bin/env python3

def displayinventory(inventory):
    print("Inventory:")
    item_total = 0
    for k, v, in inventory.items():
        item_total += inventory.get(k, 0)
        print(v, k)
    print("Total number of items: " + str(item_total))


def addtoinventory(inventory, addeditems):
    for item in addeditems:
        inventory.setdefault(item, 0)
        inventory[item] +=  1
    return inventory

stuff2 = {'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12}
inv1 = {'gold coin': 42, 'rope': 1}
dragonloot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']

inv1 = addtoinventory(inv1, dragonloot)
displayinventory(inv1)
