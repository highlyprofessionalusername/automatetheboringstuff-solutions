#!/usr/bin/env python3

birthdays = {'Nathalie': 'Oct 24', 'Danny': 'Mar 17', 'Heidi': 'Jun 23'}

while True:
    print('Enter a name: (Blank to quit)')
    name = input()
    if name == '':
        break

    if name in birthdays:
        print(birthdays[name] + ' is the birthday of ' + name)
    else:
        print('I do not have the birthday information for ' + name)
        print('What is their birthday?')
        bday = input()
        birthdays[name] = bday
        print('Birthday database updated.')
