# Practice questions, Ch 5

Q1: What does the code for an empty dictionary look like?

A1:
`emptydict = {}`

Q2: What does a dictionary value with a key `'foo'` and a value `42` look like?

A2:

```python
{'foo': '42'}
```

Q3: What is the main difference between a dictionary and a list?

A3:
Lists are ordered where dictionaries are not.

Q4: What happens if you try to access `spam['foo']` if spam is `{'bar': 100}`?

A4:
`KeyError: 'foo'` is raised as the dictionary does not have a key named `foo`.

Q5: If a dictionary is stored in `spam`, what is the difference between the expressions `'cat' in spam` and `'cat' in spam.keys()`?

A5:
There is no difference, the first is short for the second.

Q6: If a dictionary is stored in `spam`, what is the difference between the expressions `'cat' in spam` and `'cat' in spam.values()`?

A6:
The first looks for `'cat'` as a `key`. The second looks for `'cat'` as a `value`.

Q7: What is a shortcut for the following code?

```python
 if 'color' not in spam:
    spam['color'] = 'black'
```

A7:

```python
spam.setdefault('color', 'black')
```

Q8: What module and function can be used to “pretty print” dictionary values?
A8:

```python
pprint.pprint()
```