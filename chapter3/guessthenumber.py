#!/usr/bin/env python3

import random
secretnumber = random.randint(1,20)
print('I am thinking of a number between 1 and 20')

for guessestaken in range(1,7):
    print('Take a guess')
    guess = int(input())
    
    if guess < secretnumber:
        print("Your guess is too low!")
    elif guess > secretnumber:
        print("Your guess is too high")
    else:
        break

if guess == secretnumber:
    print(f'Good job! You guessed the number in {str(guessestaken)} guesses!')
else:
    print(f'Nah son, the number was {str(secretnumber)}')
