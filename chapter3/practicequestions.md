# Practice Questions, Ch 3

Q1: Why are functions advantageous to have in your programs?

A1:
Functions let you define an action once and call it as many times as you'd like, using only the function name.\
The advantages of this are making code easier to maintain and more readable with fewer changes required on update.\

Q2: When does the code in a function execute: when the function is defined or when the function is called?

A2:
When it is called.

Q3: What statement creates a function?

A3:
A function is created with the `def` statement

Q4: What is the difference between a function and a function call?

A4:
A function is a piece of code created with the `def` statement.\
A function call executes or calls that created function.\

Q5: How many global scopes are there in a Python program? How many local scopes?

A5:
Only one global scope exists, but an unlimited amount of local scopes can exist.

Q6: What happens to variables in a local scope when the function call returns?

A6:
Locally scoped variables cease to exist when the function call returns.

Q7: What is a return value? Can a return value be part of an expression?

A7:
A return value is the value that the function evaluates to. They can be part of another function call or expression

Q8: If a function does not have a return statement, what is the return value of a call to that function?

A8:
The return value is `None`

Q9: How can you force a variable in a function to refer to the global variable?

A9:
By defining it prior to the function call, and outside of the scope of a code block.
If you have more than one variable named the same, you can force the use of a global variable with the `global` statement.

Q10: What is the data type of `None`?

A10:
`NoneType`

Q11: What does the `import areallyourpetsnamederic` statement do?

A11:
It imports the module named `areallyourpetsnamederic`

Q12: If you had a function named `bacon()` in a module named `spam`, how would you call it after importing `spam?`

A12:

```python
spam.bacon()
```

Q13: How can you prevent a program from crashing when it gets an error?

A13:
By using a `try` statement, and defining an `except` statement to catch the error and defining behaviour when this occurs.

Q14: What goes in the `try` clause? What goes in the `except` clause?

A14:
The `try` clause contains the code that should be executed. The `except` clause should contain the error, as well as defining the behaviour when this error occurs.
