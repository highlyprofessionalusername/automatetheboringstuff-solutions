#!/usr/bin/env python3

def collatz(number):
    while number > 1:
        if number % 2 == 0:
            number = (number // 2)
        else: 
            number = (3 * number + 1)
        print(number)
    print('The Collatz sequence has ended')

def get_input():
    print('Input any number: ')
    while True:
        try:
           number = int(input())
           return number
        except ValueError:
           print('Please enter an integer')  

collatz(get_input())
