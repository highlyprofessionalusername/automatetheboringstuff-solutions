# Practice questions, Ch 4

Q1: What is `[]`?

A1:
An empty list.

Q2: How would you assign the value `hello` as the third value in a list stored in a variable named `spam`? (Assume spam contains `[2, 4, 6, 8, 10]`.)

A2:
`spam.insert(2, 'hello')` if you want to preserve the current value of the third index or `spam[2] = 'hello'` if you do not.

For the following three questions, let’s say spam contains the list `['a', 'b', 'c', 'd']`.

Q3: What does `spam[int(int('3' * 2) // 11)]` evaluate to?

A3:
It evaluates to the `int` version of `3`.
`('3' * 2)` evaluates to 33, because the `*` operator performed on a string concatenates the string twice, instead of multiplying it in case of an `int`.\
`33 // 11` evaluates to 3.\
The inner `int` makes sure that `('3' * 2)` returns an `int`.\
The outer `int` returns the `int` version of `3`, rather than the `str`.\

Q4: What does `spam[-1]` evaluate to?

A3:
It evaluates to `d`, since negative indexes count from the end of the list.

Q5: What does `spam[:2]` evaluate to?
A5:
It evaluates to `[a, b]`. The slice does not include the value of the index passed. So it will include index 0 and 1 only.

For the following three questions, let’s say bacon contains the list `[3.14, 'cat', 11, 'cat', True]`.

Q6: What does `bacon.index('cat')` evaluate to?

A6:
It evaluates to `1`. Lists are 0-indexed, making the second item index number 1.

Q7: What does `bacon.append(99)` make the list value in `bacon` look like?

A7:

```python
[3.14, 'cat', 11', 'cat', True, 99]
```

Q8. What does `bacon.remove('cat')` make the list value in `bacon` look like?

A8:

```python
[3.14, 11, 'cat', True, 99]
```

This is because remove will remove the first occurrence, not any following occurrence.

Q9: What are the operators for list concatenation and list replication?

A9:
`+` concatenates a list.\
`*` replicates a list `n` times, where `n` is any positive integer.\

Q10: What is the difference between the `append()` and `insert()` list methods?

A10:
`append()` adds the value at the end of the list.
`insert()` allows you to specify the index of the newly added item.

Q11: What are two ways to remove values from a list?

A11:
The `del` statement allows removal based on index.
The `remove` statement allows removal based on value.

Q12: Name a few ways that `list` values are similar to `str` values.

A12:
Strings are encapsulated by quotes `''` or `""`, lists are encapsulated by square braces `[]`.\
Strings hold a sequence of characters, lists hold a sequence of values.\
Strings and lists can be concatenated and replicated.\
Strings and lists can be sliced, and indexed.\
Strings and lists can be passed to `len()`.\
Strings and lists can be used with the `in` and `not in` operators.\

Q13: What is the difference between lists and tuples?

A13:
Lists are mutable, tuples are not.
Lists are delimited by square brackets `[]`, tuples are delimited by parentheses `()`.

Q14: How do you type the tuple value that has just the integer value 42 in it?

A14:
`(42,)`
The trailing comma specifies that we are creating a tuple, rather than a string or integer.

Q15: How can you get the tuple form of a list value? How can you get the list form of a tuple value?

A15:
The tuple form of a list value is gotten by `tuple(list)`
The list form of a tuple is gotten by `list(tuple)`

Q16. Variables that “contain” list values don’t actually contain lists directly. What do they contain instead?

A16:
They contain a list reference, this is done to save memory

Q17: What is the difference between `copy.copy()` and `copy.deepcopy()`?

A17:
`copy.copy()` copies a list, `copy.deepcopy()` will copy the list and any lists contained by the top level list as well.