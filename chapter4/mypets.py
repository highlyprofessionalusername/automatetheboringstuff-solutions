#!/usr/bin/env python3

mypets = ['Sophie', 'Pooka', 'Fat-tail']
print('Enter a pet name:')

name = input()
if name not in mypets:
    print(f'I have no pet named {name}')
else:
    print(f'{name} is my pet')
