# Practice questions, Ch 7

Q1: What is the function that creates Regex objects?
A1:

```python
re.compile()
```

Q2: Why are raw strings often used when creating Regex objects?

A2:
To avoid having to escape character classes.

Q3: What does the `search()` method return?

A3:
It returns a `match object`.

Q4: How do you get the actual strings that match the pattern from a `match object`?

A4:
Calling the `group()` method returns the strings of the matched text the `search()` method finds.

Q5: In the regex created from `r'(\d\d\d)-(\d\d\d-\d\d\d\d)'`, what does group 0 cover? Group 1? Group 2?
A5:\
Group 0 covers all groups.\
Group 1 covers `(\d{3})`\
Group 2 covers `(\d{3}-\d{4})`

Q6: Parentheses and periods have specific meanings in regular expression syntax. How would you specify that you want a regex to match actual parentheses and period characters?

A6:
By escaping the paren and period characters as follows:

```python
\. \( \)
```

Q7: The `findall()` method returns a list of strings or a list of tuples of strings. What makes it return one or the other?

A7:
If the regex has no groups, `findall()` returns a list of strings
If it does have groups, findall returns a list of tuples of strings.

Q8: What does the `|` character signify in regular expressions?

A8:
The pipe `|` allows you to match alternative patterns with a common pattern between them.
cf: `bat|man|mobile` will match `batman` and `batmobile`.

Q9: What two things does the `?` character signify in regular expressions?

A9:\
`?` flags the preceding group as an optional part of the pattern, or be used to signify non-greedy matching.
`Bat(wo)?man` will match `Batman` and `Batwoman`

Q10: What is the difference between the `+` and `*` characters in regular expressions?
A10:

`+` in a regular expression matches one or more.
`*` in a regular expression matches zero or more.

Q11.: What is the difference between `{3}` and `{3,5}` in regular expressions?

A11:\
`{3}` will match a group exactly three times. No more and no less.
It will match `HaHaHa` but not `Ha`, `HaHa` or `HaHaHaHa`
`{3,5}` will match a pattern that occurs a minimum of three and a maximum of five times.
It will match `HaHaHa`, `HaHaHaHa` and `HaHaHaHaHa`, but not `Ha`, `HaHa`, or `HaHaHaHaHaHa`

Q12: What do the `\d`, `\w`, and `\s` shorthand character classes signify in regular expressions?

A12:\
`\d` matches any numerical digit 0 through 9
`\w` matches any letter, numerical digit or underscore
`\s` matches any tab, space or newline

Q13: What do the `\D`, `\W`, and `\S` shorthand character classes signify in regular expressions?

A13:\
`\D` matches any __non-numerical__ digit from 0 to 9\
`\W` matches any character that is __not__ a letter, numerical digit or underscore\
`\S` matches any __non__ tab, space or newline\

Q14: How do you make a regular expression case-insensitive?

A14:
By using `re.I` or `IGNORECASE` as the second argument to `re.compile()`

Q15: What does the `.` character normally match? What does it match if `re.DOTALL` is passed as the second argument to `re.compile()`?

A15:
`.` matches everything except a newline, if `re.DOTALL` is passed, it will also match newlines.

Q16: What is the difference between these two: `.*` and `.*?`

A16:
`.*` will match anything except a newline, zero times or more. If more than one match is found, the longest will be returned.
`.` matches anything except a newline, `*` matches zero times or more.
`.*`? will match anything except a newline, zero times or more. If more than one match is found, the shortest will be returned.
`.` matches anything except a newline, `*` matches zero times or more, `?` signifies the previous group should match non-greedily.

Q17: What is the character class syntax to match all numbers and lowercase letters?

A17:

```python
[a-z0-9] or [0-9a-z]
```

Q18: If `numRegex = re.compile(r'\d+')`, what will `numRegex.sub('X', '12 drummers, 11 pipers, five rings, 3 hens')` return?
A18:\
'`X drummers, X pipers, five rings, X hens`'
The variable `numRegex` is assigned a regex object consisting of the raw string `'\d+`', meaning it should match a digit (`\d`) one or more times (`+`).
The `sub()` method is then called on the regex object, with the first value being the character to substitute with (`X`), and the second value being the string it should run the regex object on, and substitute where necessary. As the regex object only matches one or more digits, 'five hens' is left unchanged, while the `12`, `11` and `3` are subsituted with `X`

Q19: What does passing `re.VERBOSE` as the second argument to `re.compile()` allow you to do?

A19:
It will ignore whitespace and comments inside the regex string.

Q20: How would you write a regex that matches a number with commas for every three digits? It must match the following:

'42'

'1,234'

'6,368,745'

but not the following:

'12,34,567' (which has only two digits between the commas)

'1234' (which lacks commas)

A20:

```python
re.compile(r'(^\d{1,3})(,\d{3})*$')
```

`re.compile` creates the regex object, which consists of a raw string (`r`) and a group containing a pattern that matches 1 to 3 digits (`\d{1,3}`), with the caret (`^`) signifying this pattern must occur at the start of the string. The second group consists of a comma (`,`) followed by a group that matches exactly three digits (`\d{3}`), repeated zero or more times. The question mark at the end signifies this group is optional so the regex will still match numbers with three or fewer digits that do not call for a comma.

Q21: How would you write a regex that matches the full name of someone whose last name is Nakamoto? You can assume that the first name that comes before it will always be one word that begins with a capital letter. The regex must match the following:

'Satoshi Nakamoto'\
'Alice Nakamoto'\
'Robocop Nakamoto'\
but not the following:\
'satoshi Nakamoto' (where the first name is not capitalized)\
'Mr. Nakamoto' (where the preceding word has a nonletter character)\
'Nakamoto' (which has no first name)\
'Satoshi nakamoto' (where Nakamoto is not capitalized)\

A21:
re.compile(r'^([A-Z]\w+)(\s)(Nakamoto)') <- my answer
re.compile(r'[A-Z][a-z]*\sNakamoto') <- book answer
`re.compile` creates the regex object, which consists of a raw string and a group containing a pattern that matches any uppercase letter ([A-Z]) followed by at least one or more word characters. The next group matches any space, tab or newline character, and the final group matches the name "Nakamoto" literally.

Q22: How would you write a regex that matches a sentence where the first word is either Alice, Bob, or Carol; the second word is either eats, pets, or throws; the third word is apples, cats, or baseballs; and the sentence ends with a period? This regex should be case-insensitive. It must match the following:

'Alice eats apples.'\
'Bob pets cats.'\
'Carol throws baseballs.'\
'Alice throws Apples.'\
'BOB EATS CATS.'\

but not the following:
'Robocop eats apples.'\
'ALICE THROWS FOOTBALLS.'\
'Carol eats 7 cats.'\

A22:

```python
re.compile(r'(^alice|bob|carol) (eats|pets|throws) (apples|cats|baseballs)\.', re.I)
```

`re.compile` creates the regex object, which consists of a raw string and groups containing the words to match, separated by a pipe (`|`), signifying that the first occurrence in the group will be returned as a match. The final argument (`re.I`) signifies that the regex pattern should be case-insensitive.