#!/usr/bin/env python3

import pyperclip, re

phoneregex = re.compile(r'''(
    (\d{3}|\(\d{3}\))?  #Area Code
    (\s|-|\.)?          #Separator, optional
    (\d{3})             #First three digits
    (\s|-|\.)           #Separator, non optional
    (\d{4})             #Final four digits
    (\s*(e|x|ex|ext|ext.)\s*(\d{2,5}))? #Extension, optional
    )''', re.VERBOSE)


emailregex = re.compile(r'''(
[a-zA-Z0-9._%+=]+       #Username, all letters, all numbers, a period, underscore, hyphen, plus and percent
@                       #The @ symbol
[a-zA-Z0-9.-]+          #The domain name
(\.[a-zA-Z]{2,9})       #Dot (escaped) something
)''', re.VERBOSE)

text = str(pyperclip.paste())
matches = []
for groups in phoneregex.findall(text):
    print(groups)
    phonenum = '-'.join([groups[1], groups[3], groups[5]])
    if groups[8] != '':
        phonenum += ' x' + groups[8]
    matches.append(phonenum)
for groups in emailregex.findall(text):
    matches.append(groups[0])

if len(matches) > 0:
    pyperclip.copy('\n'.join(matches))
    print('Copied to clipboard:')
    print('\n'.join(matches))
else:
    print('No phone numbers or email addresses found.')
