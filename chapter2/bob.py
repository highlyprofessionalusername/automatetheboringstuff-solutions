#!/usr/bin/env python3

name = 'Alice'
age = 5
if name == 'Alice':
    print('Hi Alice')
elif age < 12:
    print('You are not Alice, kiddo.')
