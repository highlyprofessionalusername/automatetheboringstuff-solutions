#!/usr/bin/env python3

name = input("What's your name?" )
age = input("How old are you?" )
age = int(age)
if name == 'Alice':
    print('Hi, Alice')
elif age <12:
    print('You are not Alice, kiddo.')
else:
    print('You are neither Alice nor a little kid')
