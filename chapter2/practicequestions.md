# Practice Questions, Ch 2

Q1: What are the two values of the Boolean data type? How do you write them?

A1:
The two values are `True` and `False`

Q2: What are the three Boolean operators?

A2:
The three operators are `and`, `or` and `not`

Q3: Write out the truth tables of each Boolean operator (that is, every possible combination of Boolean values for the operator and what they evaluate to).

A3:

```python
not False = True
not True = False

True and True = True
True and False = False
False and True = False
False and False = False

True or True = True
True or False = True
False or True = True
False or False = False
```

Q4: What do the following expressions evaluate to?

```python
(5 > 4) and (3 == 5)
not (5 > 4)
(5 > 4) or (3 == 5)
not ((5 > 4) or (3 == 5))
(True and True) and (True == False)
(not False) or (not True)
```

A4:\
`(5 > 4) and (3 == 5)` evaluates to `False` (`True and False` evaluates to `False`)\
`(5 > 4)` evaluates to `True` (Five is greater than four)\
`(3 == 5)` evaluates to `False`. (Three does not equal five)\
`True and False` evaluates to `False`\

`not (5 > 4)` evaluates to `False`\
`(5 > 4)` evaluates to True (Five is greater than four)\
`not True` evaluates to `False`\

`(5 > 4) or (3 == 5)` evaluates to `True`\
`(5 > 4)` evaluates to `True` (Five is greater than four)\
`(3 == 5)` evaluates to `False` (Three does not equal five)\
`True or False` evaluates to `True`\

`not ((5 > 4) or (3 == 5))` evaluates to `False`\
`(5 > 4)` evaluates to `True` (Five is greater than four)\
`(3 == 5)` evaluates to `False`. (Three does not equal five)\
`True or False` evaluates to `True`\
`not True` evaluates to `False`\

`(True and True)` and `(True == False)` evaluates to `False`\
`(True and True)` evaluates to `True`\
`(True == False)` evalutes to `False`\
`True and False` evaluates to `False`\

`(not False)` or `(not True)` evaluates to `True`\
`(not False)` evaluates to `True`\
`(not True)` evaluates to `False`\
`True or False` evaluates to `True`\

Q5: What are the six comparison operators?

A5:
`==`, equals to\
`!=`, not equal to\
`<`, less than\
`>`, greater than\
`<=`, less than or equal to\
``>=`, greater than or equal to\

Q6: What is the difference between the equal to operator and the assignment operator?

A6:
The assignment operator `=` assigns a value to a variable.
The equal to operator `==` compares the variables or values on one side to the value or variable on the other side.

Q7: Explain what a condition is and where you would use one.

A7:
A condition will only run if its prerequisites are met. It will not run at all if the prerequisite is not met.
You would use one in a scenario where multiple outcomes should result in different actions.

Q8:s Identify the three blocks in this code:

```python
spam = 0
if spam == 10:
    print('eggs')
    if spam > 5:
        print('bacon')
    else:
        print('ham')
    print('spam')
print('spam')
```

A8:
The first (outer) block is from line 2 to line 8:

```python
if spam == 10:
    print('eggs')
    if spam > 5:
        print('bacon')
    else:
        print('ham')
    print('spam')
```

The second (inner) block is from line 4 to line 5:

```python
    if spam > 5:
        print('bacon')
```

The third (inner) block is from line 6 to line 7:

```python
    else:
        print('ham')
```

Q9: Write code that prints `Hello` if `1` is stored in `spam`, prints `Howdy` if `2` is stored in `spam`, and prints `Greetings!` if anything else is stored in `spam`.
A9:
see `spam.py`:

```python
spam = input('Type something here' )
if spam == str('1'):
    print('Hello')
elif spam == str('2'):
    print('Howdy')
else:
    print('Greetings!')
```

In this example, the `spam` variable gets input, which is why we need to make sure to check for a string version of `1` or `2`, as it otherwise assumes an `int` version. If the spam variable is assigned the variable `1` or `2`, the string check is not necessary.

Q10: What can you press if your program is stuck in an infinite loop?

A10:
Ctrl+C

Q11: What is the difference between `break` and `continue`?

A11:
`Break` exits a loop.
`Continue` jumps back to the start of the loop and re-evaluates (moves to the next iteration of the loop)

Q12: What is the difference between `range(10)`, `range(0, 10)`, and `range(0, 10, 1)` in a `for` loop?

A12:
`range(10)` returns up to but not including the number provided (10)
`range(0, 10)` returns the same, but explicitly provides the starting number in the range
`range(0, 10, 1)` returns the same, but both explicitly provided the starting number of the range, as well as the step argument (the `1` in the third place). The step argument specified by how much the value will increment.

Q13: Write a short program that prints the numbers 1 to 10 using a for loop. Then write an equivalent program that prints the numbers 1 to 10 using a while loop.

A13:
see `rangeten.py`:

```python
for i in range(1, 11):
    print(i)
```

see `whilerangeten.py`:

```python
i = 1
while i <= 10:
    print(i)
    i = i + 1
```

Q14: If you had a function named `bacon()` inside a module named `spam`, how would you call it after importing `spam`?
A14:

```python
spam.bacon()
```