#!/usr/bin/env python3

def printpicnic(itemsdict, leftwidth, rightwidth):
    print('PICNIC ITEMS'.center(leftwidth + rightwidth, '-'))
    for k, v in itemsdict.items():
        print(k.ljust(leftwidth, '.') + str(v).rjust(rightwidth))
picnicitems = {'sandwiches': 4, 'apples': 80, 'cups': 4, 'cookies': 1000}
printpicnic(picnicitems, 12, 5)
printpicnic(picnicitems, 20, 6)
