# Practice questions, Ch 6

Q1: What are escape characters?

A1:
Escape characters allow you to treat the character after it as a special character, rather than the literal one.
`\t` represents a tab, the \ tells python the char following it is special, even if it can normally not be part of a string.

Q2: What do the `\n` and `\t` escape characters represent?

A2:
`\n` is a Newline\
`\t` is a Tab

Q3: How can you put a `\` backslash character in a string?

A3:
By typing `\\`, as the first slash will escape the one that follows it.

Q4: The string value `"Howl's Moving Castle"` is a valid string. Why isn’t it a problem that the single quote character in the word `Howl's` isn’t escaped?

A4:
Because the string is encapsulated by double quotes `""` rather than single quotes `''`.

Q5: If you don’t want to put `\n` in your string, how can you write a string with newlines in it?

A5:
By enclosing the string inside of triple double quotes like this:

```python
"""
H
e
l
l
o
"""
```

Q6: What do the following expressions evaluate to?

```python
'Hello world!'[1]
'Hello world!'[0:5]
'Hello world!'[:5]
'Hello world!'[3:]
```

A6:\
`'Hello world!'[1]` evaluates to 'e', as strings are 0-indexed.\
`'Hello world!'[0:5]` evaluates to 'Hello', as the slice will return everything up to but not including the ending index.\
`'Hello world!'[:5]` evaluates to 'Hello', as the slice will return everything up to but not including the ending index.\
`'Hello world!'[3:]` evaluates to 'lo world!', as the slice will return everything from, but not including the starting index.\

Q7: What do the following expressions evaluate to?

```python
'Hello'.upper()
'Hello'.upper().isupper()
'Hello'.upper().lower()
```

A7:\
`'Hello'.upper()` evaluates to 'HELLO'\
`'Hello'.upper().isupper()` evaluates to `True`, as the `.isupper()` method is called after the `upper()` method.\
`'Hello'.upper().lower(`) evaluates to 'hello' as the `upper()` method is called prior to the `lower()` method, which returns lowercase.\

Q8: What do the following expressions evaluate to?

```python
'Remember, remember, the fifth of November.'.split()
'-'.join('There can be only one.'.split())
```

A8:\
`['Remember,', 'remember,', 'the', 'fifth', 'of', 'November.']`, as split without arguments will split on space.\
`'There-can-be-only-one.'`\

Q9: What string methods can you use to right-justify, left-justify, and center a string?

A9:

```python
rjust()
ljust()
center()
```

Q10: How can you trim whitespace characters from the beginning or end of a string?

A10:

```python
strip()
lstrip()
rstrip()
```

`strip()` will remove whitespace at the beginning and end.\
`lstrip()` will remove whitespace at the beginning.\
`rstrip()` will remove whitespace at the end.\