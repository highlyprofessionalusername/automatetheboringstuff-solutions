#! /usr/bin/env python3

PASSWORDS = {'email': 'F7minlBDDuvMJuxESSKHFhTxFtjVB6',
             'forums': 'VmALvQyKAxiVH5G8v01if1MLZF3sdt',
             'luggage': '864'}

import sys
import pyperclip
if len(sys.argv) <2:
    print('Usage: python3 pw.py [account] - copy account password')
    sys.exit()

account = sys.argv[1] #First CLI arg is the account name

if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print (f'Password for {account} copied to clipboard')
else:
    print(f'There is no account named {account}')
