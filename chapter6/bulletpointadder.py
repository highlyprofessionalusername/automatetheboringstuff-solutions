#!/usr/bin/env python3

import pyperclip
text = pyperclip.paste()

lines = text.split('\n')
for i in range(len(lines)): #loop through all index in lines list
    lines[i] = '* ' + lines[i] #Add the asterisk
text = '\n'.join(lines)
pyperclip.copy(text)
