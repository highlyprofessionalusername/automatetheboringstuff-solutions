#!/usr/bin/env python3

tabledata = [['apples', 'oranges', 'cherries', 'banana'],
             ['Alice', 'Bob', 'Carol', 'David'],
             ['dogs', 'cats', 'moose', 'goose']]

def itemlength(string_list):
    longest = 0
    for string in string_list:
        if len(string) > longest:
            longest = len(string)
    return longest

def printtable(table):
    colwidths = populatecolwidths()
    for itm in range(0, len(table[0])):
        for lst in range(0, len(table)):
            print(table[lst][itm].rjust(colwidths[lst]+1), end="")
        print()

def populatecolwidths():
    columns = []
    for i in range(0, len(tabledata)):
        columns.append(itemlength(tabledata[i]))
    return columns

printtable(tabledata)
